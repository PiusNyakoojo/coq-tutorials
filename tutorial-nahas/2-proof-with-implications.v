(*
Proofs with -> (Implications)
*)

Theorem forward_small : (forall A B : Prop, A -> (A -> B) -> B).

Proof.
  intros A.
  intros B.
  intros suppose_A.
  intros suppose_A_implies_B.
  pose (proven_B := suppose_A_implies_B suppose_A).
  exact proven_B.
Qed.


(*
The "pose" tactic is used in a forward proof.

Use this tactic to create the result type with multiple
implications. For example, if you have 

<hypothesis_name> : <type_1> -> <type_2> ... -> <result_type>
(or the equivalent forall statements)

Use "pose" as follows:

pose (thing_of_result_type := thing_of_type_2 thing_of_type_1).

A "forward proof" creates larger and more complex hypotheses
in the context until one matches the goal.

A "backward proof" breaks the goal into smaller and simpler
subgoals until they're trivial.
*)

(*
We could have ended the proof with

"exact (suppose_A_implies_B suppose_A)." but it is easier to 
read when we end with "exact proven_B"
*)

Theorem backward_small : (forall A B : Prop, A -> (A->B) -> B).

Proof.
  intros A B.
  intros suppose_A suppose_A_implies_B.
  refine (suppose_A_implies_B _).
    exact suppose_A.
Qed.

(*
The command "refine" let's us create the proof of B without
specifying the argument of type A. The parentheses are 
necessary for it to parse correctly. This solves our current
subgoal of type B and the unspecified argument - represented
by the underscore ("_") - become a new "child" subgoal.

Since proving "A" is a child subgoal of having "B", we indent
the tactics used to solve it.

General rule:
If you have subgoal "<goal_type>" AND have hypothesis
"<hypothesis_name>: <type_1> -> <type_2> ... -> <type_N>
-> <goal_type>", then use tactic

"refine (<hypothesis_name> _ ...)." with N underscores.

The important thing to take away from this proof is that
we changed the subgoal. That's what happens in a backward
proof. You keep changing the subgoal to make it smaller
and simpler. "A" doesn't seem much smaller or simpler than
"B", but it was.

refine is like a function that accepts the name of a function,
i.e. the proposition "suppose_A_then_B" and a list of underscores.
refine returns a number of subgoals equal to the length of the
list.

function arg1 arg2

The indentations show that there is a branch in the proof created
by the refine statement.
*)

Theorem backward_large :
  (forall A B C : Prop, A -> (A -> B) -> (B -> C) -> C).

Proof.
  intros A B C.
  intros suppose_A suppose_A_implies_B suppose_B_implies_C.
  refine (suppose_B_implies_C _).
    refine (suppose_A_implies_B _).
      exact suppose_A.
Qed.


Theorem backward_huge :
  (forall A B C : Prop, A -> (A->B) -> (A->B->C) -> C).

Proof.
  intros A B C.
  intros suppose_A suppose_A_then_B suppose_A_then_B_then_C.
  refine (suppose_A_then_B_then_C _ _).
    exact suppose_A.

  refine (suppose_A_then_B _).
    exact suppose_A.
Qed.

Theorem forward_huge :
  (forall A B C : Prop, A -> (A->B) -> (A->B->C) -> C).

Proof.
  intros A B C.
  intros suppose_A suppose_A_then_B suppose_A_then_B_then_C.
  pose (proof_of_B := suppose_A_then_B suppose_A).
  pose (proof_of_C := suppose_A_then_B_then_C suppose_A proof_of_B).
  exact proof_of_C.
Show Proof.
Qed.