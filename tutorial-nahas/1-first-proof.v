(* Your First Proof! *)

(*
We'll begin by proving the proposition:

For all things you could prove, if you have a proof of it,
then you have a proof of it.
*)

Theorem my_first_proof : (forall A : Prop, A -> A).

Proof.
  intros A.
  intros suppose_A.
  exact suppose_A.
Qed.

(*
Coq uses 3 different "languages" and you can see them asll here in the
proof.

- The "vernacular" language manages definitions, and each of its 
commands starts with a capital letter: "Theorem", "Proof", and "Qed".

- The "tactics" language is used to write proofs, and its commands start
with a lower-case letter: "intros" and "exact".

- The "Gallina" language is used to express what you want to prove, and
its expressions use lots of operators and parentheses:

"(forall A : Prop, A -> A)"
*)

(*
The tactic "intros" takes "forall" off of the subgoal, changes its variable
into a hypothesis in the context, and names the hypothesis.

"->" is shorthand for "forall"
"B -> C" means "(forall something_of_type_B : B, C)".
"A -> A" means "(forall something_of_type_A : A, A)".

In Coq, "A : Prop" means you have something names "A" of type "Prop".
In the future, you will see "0 : nat" which means "0" of type "nat"
(natural numbers) and you will see "true : bool" which means "true"
of type "bool" (boolean or true-false value).

In some places, you will see "A B C : Prop", which means "A", "B", "C"
all have type "Prop".
*)

(*
It is vitally important that you do not think of a Prop as being true
or false. A Prop either has a proof or it does not have a proof.

Godel shattered mathematics by showing that some true propositions can
never be proven.

Tarski went even further and showed that some propositions cannot
even be said to be true or false!!!

Coq deals with these obstacles in modern mathematics by restricting
Prop to being either proven or unproven, rather than true or false.
*)

(*
The tactic "exact" is used when the hypothesis matches your subgoal.
*)